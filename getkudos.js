/**
 * @file
 * JavaScript for the GetKudos module.
 */

(function ($) {

Drupal.behaviors.getKudos = {
  attach: function (context, settings) {
    $.ajax({
      cache: true,
      dataType: 'script',
      success: function(script, textStatus, jqXHR) {
        if (settings.getKudos.floatingWidget) {
          getkudos('create', settings.getKudos.brand);
        };
        if (settings.getKudos.inlineWidget) {
          getkudos('parse');
        };
      },
      url: '//static.getkudos.me/widget.js',
    })
  }
};

})(jQuery);
