<?php

/**
 * @file
 * The GetKudos module.
 */

/**
 * Implements hook_block_info().
 */
function getkudos_block_info() {
  $blocks['getkudos_inline_widget'] = array(
    'info' => t('GetKudos Inline Widget'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function getkudos_block_configure($delta = '') {
  $form = array();
  if ($delta == 'getkudos_inline_widget') {
    $form['getkudos_brand'] = array(
      '#type' => 'textfield',
      '#title' => t('GetKudos Brand'),
      '#description' => t('Vanity url for your brand (getkudos.me/<strong>[your-brand]</strong>).'),
      '#default_value' => variable_get('getkudos_brand'),
      '#required' => TRUE,
    );

    $url = 'https://getkudos.me/blog/2014/02/27/dress-getkudos-inline-widget';
    $form['getkudos_pagination'] = array(
      '#type' => 'fieldset',
      '#title' => t('GetKudos widget pagination'),
      '#description' => t('See !url for detail', array('!url' => l($url, $url))),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['getkudos_pagination']['getkudos_pagination_init'] = array(
      '#type' => 'textfield',
      '#title' => t('Pagination init'),
      '#description' => t('Number of Kudos displayed'),
      '#default_value' => variable_get('getkudos_pagination_init'),
      '#element_validate' => array('getkudos_validate_pagination'),
    );

    $form['getkudos_pagination']['getkudos_pagination_each'] = array(
      '#type' => 'textfield',
      '#title' => t('Pagination each'),
      '#description' => t('Additional number of Kudos displayed'),
      '#default_value' => variable_get('getkudos_pagination_each'),
      '#element_validate' => array('getkudos_validate_pagination'),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function getkudos_block_save($delta = '', $edit = array()) {
  if ($delta == 'getkudos_inline_widget') {
    variable_set('getkudos_brand', $edit['getkudos_brand']);
    variable_set('getkudos_pagination_init', $edit['getkudos_pagination_init']);
    variable_set('getkudos_pagination_each', $edit['getkudos_pagination_each']);
  }
}

/**
 * Implements hook_block_view().
 */
function getkudos_block_view($delta = '') {
  $block = array();

  if ($delta == 'getkudos_inline_widget') {
    $block['subject'] = NULL;
    $block['content'] = getkudos_inline_widget();
  }

  return $block;
}

/**
 * Implements hook_init().
 */
function getkudos_init() {
  $brand = variable_get('getkudos_brand');

  if (!empty($brand)) {
    $data = array(
      'getKudos' => array(
        'brand' => $brand,
        'floatingWidget' => FALSE,
        'inlineWidget' => FALSE,
      ),
    );

    if (variable_get('getkudos_floating_widget_display') && _getkudos_page_match()) {
      $data['getKudos']['floatingWidget'] = TRUE;
    }

    drupal_add_js($data, array('type' => 'setting'));
    drupal_add_js(drupal_get_path('module', 'getkudos') . '/getkudos.js', array('scope' => 'footer'));
  }
}

/**
 * Implements hook_menu().
 */
function getkudos_menu() {
  $items['admin/config/services/getkudos'] = array(
    'title' => 'GetKudos',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('getkudos_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'getkudos.admin.inc',
  );

  return $items;
}

/**
 * Render API callback: Validates GetKudos pagination field.
 *
 * Ensure that pagination value is integer.
 *
 * This function is assigned as an #element_validate callback in
 * getkudos_block_configure().
 */
function getkudos_validate_pagination($element, &$form_state, $form) {
  if (filter_var($element['#value'], FILTER_VALIDATE_INT) === FALSE) {
    form_error($element, t('%field must be integer.', array('%field' => $element['#title'])));
  }
}

/**
 * Display GetKudos inline widget.
 *
 * @return array
 *   A render array for GetKudos inline widget.
 */
function getkudos_inline_widget() {
  $setting = array(
    'data' => array(
      'getKudos' => array(
        'inlineWidget' => TRUE,
      ),
    ),
    'type' => 'setting',
  );

  $widget = array(
    '#type' => 'html_tag',
    '#tag' => 'div',
    '#value' => '',
    '#attributes' => array(
      'class' => array('getkudos-inline'),
      'data-site-name' => variable_get('getkudos_brand'),
      'data-layout' => 'inline',
    ),
    '#attached' => array(
      'js' => array(
        $setting,
      ),
    ),
  );

  $pagination_init = variable_get('getkudos_pagination_init');
  $pagination_each = variable_get('getkudos_pagination_each');

  if (!empty($pagination_init)) {
    $widget['#attributes']['data-pagination-init'] = $pagination_init;
  }
  if (!empty($pagination_each)) {
    $widget['#attributes']['data-pagination-each'] = $pagination_each;
  }

  return $widget;
}

/**
 * Determine if GetKudos floating widget is active in the current page.
 *
 * @return
 *   TRUE if active, FALSE otherwise.
 */
function _getkudos_page_match() {
  $pages = variable_get('getkudos_floating_widget_pages', 'admin/*');
  $visibility = variable_get('getkudos_floating_widget_visibility', 0);

  if ($pages) {
    $path = drupal_get_path_alias($_GET['q']);
    $page_match = drupal_match_path($path, $pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }

    return !($visibility xor $page_match);
  }
  else {
    return !$visibility;
  }
}

