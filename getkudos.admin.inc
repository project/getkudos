<?php

/**
 * @file
 * Administration forms for the GetKudos module.
 */

/**
 * Form constructor for GetKudos settings form.
 *
 * @ingroup forms
 */
function getkudos_settings($form, &$form_state) {
  $states = array(
    'invisible' => array(
      ':input[name="getkudos_floating_widget_display"]' => array('checked' => FALSE),
    ),
  );

  $form['getkudos_brand'] = array(
    '#type' => 'textfield',
    '#title' => t('GetKudos Brand'),
    '#description' => t('Vanity url for your brand (getkudos.me/<strong>[your-brand]</strong>).'),
    '#default_value' => variable_get('getkudos_brand'),
  );

  $form['getkudos_floating_widget_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('GetKudos floating widget'),
    '#description' => t('Display the GetKudos floating widget'),
    '#default_value' => variable_get('getkudos_floating_widget_display', 0),
  );

  $form['getkudos_floating_widget_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show GetKudos floating widget on specific pages'),
    '#default_value' => variable_get('getkudos_floating_widget_visibility', 0),
    '#options' => array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.')),
    '#states' => $states,
  );

  $form['getkudos_floating_widget_pages'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('getkudos_floating_widget_pages', 'admin/*'),
    // '#rows' => 3,
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are  %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#states' => $states,
    // '#wysiwyg' => FALSE,
  );

  return system_settings_form($form);
}
