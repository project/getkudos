CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The GetKudos module integrate https://getkudos.me to your drupal site
installation.

"GetKudos is a business tool for collecting and displaying customer reviews and
testimonials, optimized for authenticity in today's age of social media.
We aim for a fuss-free solution without you requiring engineering help."

 * For a full description of the module, visit the project page:
  https://drupal.org/project/getkudos
 * To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/getkudos

INSTALLATION
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
 * Enter your GetKudos brand  in Administration » Configuration » Web Services »
  GetKudos. If you want display floating widget check GetKudos floating widget
  checkbox. Customize the path where you want or not the floating widget
  displayed.
 * To display GetKudos inline widget, enable its block in Administration »
  Structure » Blocks.
 * To embed GetKudos inline widget in your theme's template,
  call getkudos_inline_widget() and wrapped it with drupal_render() or render.

MAINTAINERS
-----------
Current maintainers:
 * Luhur Abdi Rizal (el7cosmos) - https://drupal.org/user/251087
